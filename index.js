//   Lesson Proper
//   Conditional statements
//   - a conditional statement is one of the key features of a programming language
//  There are three types of Conditional statements:
// if else statements; switch statements; try-catch-finally statements

// operators
//  - allow programming languages to execute or evaluations

// assignment operator
// - assign a value to a variable 
// Basic assignment operator (=)

let variable = "Initial value";

//mathematical operators (+), (-), (/), (*), (%)
// whenever you've used a mathematical operators, a value is returned, it is only up to us if we save that returned value.
//addition, subtraction, multiplication, division assignment operators allows us to assign the result of the operation to the value of the left operand. It allos us to save the result of a mathematical operation to the left operand. 

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

/*
Addition assignment operator: (+=)
left operand
- is the variable or value of the left side of the operator
right operand
- is the variable or value of the right side of the operator 

ex. sum = num1 + num4; > re-assigned the value of the sum with the result of num1 + num4
*/

let sum = num1 + num4;
// let num1 = num1 + num4; can be shortened
 num1 += num4; // same as the above

console.log(num1);

num2 += num3; 

console.log(num2);

num1 += 55;
console.log(num1);

// previous right should not be affected/re-assigned

let string1 = "Boston";
let string2 = "Celtix";
// string1 = string1 + string2

string1 += string2;
console.log(string1); // BOstonCletix  results in concatenation between 2 strings and saves the result in the left operand
console.log(string2); //Celtix - unaffected

//15 += num1; = produces an error, because we do not use assignment operator when the left operand is just a value/data

//subtraction assignment operator (-=)
//same with addition assignment operator

num1 -= num2;
console.log("Result of sub ass ops: " + num1)

//multiplication assignment operator: (*=)

num2 *= num3;
console.log(num2);

//division multipication assignment operator (/=)

num4 /= num1;
console.log(num4);

//[SECTION]

//Arithmetic operators
let x = 1397;
let y = 7831;

let sum1 = x + y;
console.log(sum1);

let diff = y - x;
console.log(diff);

let product = x * y;
console.log(product);

let quotient = y / x;
console.log(quotient);

let remainder = y % x;
console.log(remainder);

 // multiple operators and parenthesis
 /*
when multiple operators are applied in a single statement, it follows the PEMDAS rule
the operations were done in the followig=ng order:
1. 3 *4 = 12
2. 12/ 5 = 2.4
3. 1 + 2 = 3
4. 3 - 2.4 = 0.6
 */
 let mdas = 1 + 2 - 3 *4 /5;
 console.log(mdas);

 let pemdas = 1 + (2 - 3) * (4 / 5);
 console.log(pemdas);// 0.19  by adding parenthesis, the order of operation changed its priority following the pemdas rule

 // increment and decrement
// increment and decrement is adding or subtracting 1 from the variable and re-assigning the new value to the variable where the increment or decrement was used.

// 2 kinds of incrementation: pre-fix and postfix

let z = 1;

//pre-fix incrementation - increment first then return value
++z;
console.log(z);

//postfix incrementation - return the value first, then increment
z++; 
console.log(z);

// decrement - same as increment, the difference is decrement decreases the value.

// comparison operator
// - are used to compare values of the left operand and right operand
// returns boolean
//equally or loose equally operator (==)
// strict equality (===)

console.log(1==1); //true

let isSame = 55 == 55;
console.log(isSame);

console.log(1=='1'); // true - loose equality operator priority is the sameness of the value because with loose equality, forced coercion is done before comparison- JS forcibly changes the data type to the operands 
console.log(0==false);//true - with force coercion, false was converted into a number results into NaN so therefore, 1 is not equal to NaN
console.log(1==true);//true   
console.log("false"==false);//false 
console.log(true=="true"); //false

//in loose equality (==) - values are compared and types if operands do not have the same types, it will be forced coerced/type coerced beforre comparison value
// if either the operand is a number if boolean, the operands are converted into numbers.

console.log(true=="1");// true - true was coerced into a number = 1, "1" was also converted into a number = 1

//strict equality
console.log(true === "1");//false
//checks both value and type
console.log(true === "1"); //false - operands have the same value but different types


// inequality and strict inequality (!=)
// loose inequality operators
// -checks whether the operands are NOT equal or have different values
// - will do force coercion if the operands have differences

console.log('1'!= 1); //false - both values were converted into a number = 1

console.log('James'!= "John"); // true - not equal to each other

console.log(1 != true);// false - true was converted into a number equal to 1

//strict inequality (!==) - same with strict equality but the other way around

console.log('5'!== 5);// true - operands are inequal

let name1 = "Jin";
let name2 = "Jimin";
let name3 = "Jungkook";
let name4 = "V";

let number1 = 50;
let number2 = 60;
let numString1 = "50";
let numString2 = "60";

console.log(numString1 == number1);// true
console.log(numString1 === number1); //false
console.log(numString1 != number1); //false
console.log(name4 != "num3"); //true
console.log(name3 == "Jungkook"); //true
console.log(name1 === "Jin"); //true

//Relational comparison operators
// a comparison operator compares its operand and returns a boolean value based on whether the comparison is true

let a = 500;
let b = 700;
let c = 8000;

let numString3 = "5500";

//Greater than (>)
console.log(a > b);
console.log(c > y);

//less than (<)
console.log(c < a);// false
console.log(b < b);// false
console.log(a < 1000);//true
console.log(numString3 < 1000); //false - forced coercion to change the string to number
console.log(numString3 < 6000);//true
console.log(numString3 < "Jose");//true - "5500" < "Jose" - that is erratic(erratic means unpredictable)

//Greater than or equal to (>=)
console.log(c >= 1000);//false
console.log(b >= a);// true

//Less than or equal to (<=)
console.log(a <= b);//true
console.log(c <= a ); //false

//Logical operators
// AND operators (&&)
// - both side of the operands must be true otherwise false

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authority = isAdmin && isRegistered;
console.log(authority);//false

let authority2 = isLegalAge && isRegistered;
console.log(authority2);//true

let reqLevel = 95;
let reqAge = 18;

let authority3 = isRegistered && reqLevel === 25;
console.log(authority3);//false

let authority4 = isLegalAge && isRegistered && reqLevel === 95;
console.log(authority4);//true

let userName = "gamer2001";
let userName2 = "shadow1991"
let userAge = 15;
let userAge2 = 30;

let registration1 = userName.length > 8 && userAge >= reqAge;
//.length is a property of string which determines the number of characters in a string.
console.log(registration1);//false - userAge did not  meet the req Age

let registration2 = userName2.length > 8 && userAge2 >= reqAge;
console.log(registration2);//true

//OR operator (||)
//returns true if at least one of the operands returned true
let userLvl = 100;
let userLvl2 = 65;
let guildReq1 = isRegistered || userLvl >= reqLevel || userAge >= reqAge;
console.log(guildReq1);//true

//Conditional statement
//if -else statement

let userName3 = "Crusader_1993";
let userAge3 = 20;
let userLvl3= 25;


/*if(true){
	alert("wwaaa")
};*/

if(userName3.length >10){
	console.log("Welcome to SAO!");
};

if(userLvl3 >= reqLevel){
	console.log("You're qualified to join the Guild");
};
if(userName3.length >10 && userLvl3 <= 25 && userAge3 >= reqAge){
	console.log("Thank you for joining LC Guild");
}
else{ //run else statement if the above conditions were not met
	console.log("You are too sane to join LC");
};

//else-if - executes a statement if the original condition is false but has another condition resulted to true

//function and if else
//typeof- returns a string which tells the type of data that follows it
function addNum (num1, num2){
	if(typeof num1 === 'number' && typeof num2 === 'number'){
		console.log("run only if both are true");
		console.log(num1 + num2);
	}
	else{
	console.log('either operands is false');
	}
}
addNum(5, "2");


function login(username, password) {
	if (typeof username === 'string' && typeof password === 'string') {
		console.log("Both arguments are strings.");
		//Nested if-else = will run if the parent if statement is able to agree to accomplish its condition

		if(password.length >= 8){
			console.log("Welcome");
		}
		else if(username.length <= 8){
			alert("Username is too short");
		}
		else if(password.length <= 8){
			alert("Password is too short");
		}
		else{
		alert("Credentials too short");
		}
};
};

login("ABCEFGHI", "itoyungpasswordko");

function shirtColor(day) {
if (typeof day === 'string') {
	if(day.toLowerCase() === 'monday'){
			alert("Today is Monday, wear Black");
		} else if(day.toLowerCase() === "tuesday"){
			alert("Today is Tuesday, wear Green");
		} else if(day.toLowerCase() === "wednesday"){
			alert("Today is Wednesday, wear Yellow");
		} else if(day.toLowerCase() === "thursday"){
			alert("Today is Thursday, wear Red");
		} else if(day.toLowerCase() === "friday"){
			alert("Today is Friday, wear Violet");
		} else if(day.toLowerCase() === "saturday"){
			alert("Today is Saturday, wear Blue");
		} else if(day.toLowerCase() === "sunday"){
			alert("Today is Sunday, wear White");
		} else{
			alert("Invalid Input. Enter a valid day of the week")
		}
}
else{
	alert("Invalid Input. Please input a string ")
}
}

shirtColor("monday");

